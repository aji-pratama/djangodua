from django.db import models

class Maskapai(models.Model):
    index = models.IntegerField(max_length=30)
    airline = models.CharField(max_length=30)
    time = models.CharField(max_length=30)
    period = models.CharField(max_length=30)
    airper = models.CharField(max_length=50)

    def __str__(self):
        return self.airline

class Pilihan(models.Model):
    airline = models.CharField(max_length=30, unique=True, null=True)
