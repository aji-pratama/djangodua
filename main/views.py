from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext
from main.models import Maskapai, Pilihan
import time

def index(request):
    maskapai = Maskapai.objects.all().order_by('index')
    pilihan = Pilihan.objects.all()
    return render_to_response('index.html', locals(), context_instance=RequestContext(request))

def insert(request):
        maskapai = Maskapai.objects.all().order_by('index')
        pilihan = Pilihan.objects.all()

        airline = str(request.POST.get('airline'))
        times = str(request.POST.get('time'))
        timet = request.POST.get('time')

        try:
            if (timet >= '00:00') and (timet <= '10:00'):
                period = 'pagi'
            elif (timet >= '10:01') and (timet <= '18:00'):
                period = 'siang'
            elif (timet >= '18:01') and (timet <= '23:59'):
                period = 'malam'

            #data post index
            index_data = Maskapai.objects.all().count() + 1
            findex = Maskapai.objects.filter(index=index_data)
            if (findex):
                index = index_data + 1
            else:
                index = index_data

            airper = airline + period
            tim = time.strptime(times,'%H:%M')

            insert_data = Maskapai(index=index, airline=airline, time=times, period=period, airper=airper)
            insert_data_pilihan = Pilihan(airline=airline)

        except Exception, err:
            print err
            return render_to_response('index.html', locals(), context_instance=RequestContext(request))

        #Insert data Pilihan jiga belum ada, jika sudah berarti tidak diinsert
        pilihans = Pilihan.objects.filter(airline=airline)
        if (pilihans):
            pass
        else:
            insert_data_pilihan.save()

        #Insert data maskapai
        insert_data.save()

        return render_to_response('index.html', locals(), context_instance=RequestContext(request))

def delete(request, index):
    #get data by index
    maskapai_del = Maskapai.objects.get(index=index)
    #airline by index got
    mask_sel = maskapai_del.airline #airline
    #select table pilihan by mask_sel
    pilihans = Pilihan.objects.filter(airline=mask_sel)
    #deleted maskapai
    maskapai_del.delete()

    #Conditional, is exist?
    maskapai = Maskapai.objects.filter(airline=mask_sel)
    if (maskapai):
        pass
    else:
        pilihans.delete()

    #Merubah Index
    del_index = maskapai_del.index
    maskapai = Maskapai.objects.all()
    for i in maskapai:
        if i.index > del_index:
            ind = i.index - 1
            m = Maskapai.objects.get(index=i.index)
            m.index = ind
            m.save()

    #Models variables
    pilihan = Pilihan.objects.all()
    maskapai = Maskapai.objects.all().order_by('index')
    return render_to_response('index.html', locals(), context_instance=RequestContext(request))

def up(request, index):
    #data by index(get)
    indeks = Maskapai.objects.get(index=index)

    if (indeks > 1):
        indexs = indeks.index

        index_up = indexs - 1
        index_down = index_up + 1

        maskapaid = Maskapai.objects.get(index=index_up)

        indeks.index = index_up
        maskapaid.index = index_down
        indeks.save()
        maskapaid.save()
    else:
        pass

    pilihan = Pilihan.objects.all()
    maskapai = Maskapai.objects.all().order_by('index')
    return render_to_response('index.html', locals(), context_instance=RequestContext(request))

def down(request, index):
    #data by index(get)
    indeks = Maskapai.objects.get(index=index)
    indexs = indeks.index

    index_up = indexs + 1
    index_down = index_up - 1

    maskapaid = Maskapai.objects.get(index=index_up)

    i = Maskapai.objects.all().count()
    if (indeks > i):
        indeks.index = index_up
        maskapaid.index = index_down
        indeks.save()
        maskapaid.save()
    else:
        pass

    pilihan = Pilihan.objects.all()
    maskapai = Maskapai.objects.all().order_by('index')
    return render_to_response('index.html', locals(), context_instance=RequestContext(request))
