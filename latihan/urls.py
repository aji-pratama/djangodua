from django.conf.urls import patterns, include, url
from django.conf import settings

urlpatterns = patterns('',
    url(r'^$', 'main.views.index', name='index'),
    url(r'^insert', 'main.views.insert', name='insert'),
    url(r'^delete/(?P<index>\d+)/$', 'main.views.delete', name='delete'),
    url(r'^up/(?P<index>\d+)/$', 'main.views.up', name='up'),
    url(r'^down/(?P<index>\d+)/$', 'main.views.down', name='down'),

)
